import { GET_CONDITION_API } from "../constants/product.action";

const initialState = {
    condition: null,
    status: false,

}
export default function productReducer(state = initialState, action){
        switch (action.type) {
            case GET_CONDITION_API:
                state.condition = action.payload;
                state.status = true
                break;
            default:
                break;
        }
        console.log(state)
    return {...state}
}