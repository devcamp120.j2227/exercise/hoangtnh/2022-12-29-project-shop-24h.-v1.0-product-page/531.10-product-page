import Categories from "./Components/bodyComponent/bodyContent/Categories";
import HomePageShop24h from "./pages/homePage";
import MoreProduct from "./pages/moreProducts";


const routerList = [
    {path: "/", element: <HomePageShop24h/>},
    {path:"/Categories", element:<Categories/>},
    {path:"/Categories/More", element:<MoreProduct/>}
]
export default routerList;