import { GET_CONDITION_API } from "../constants/product.action";

export default function getCondition  (data) {
    return{
        type: GET_CONDITION_API,
        payload: data
    }
};
