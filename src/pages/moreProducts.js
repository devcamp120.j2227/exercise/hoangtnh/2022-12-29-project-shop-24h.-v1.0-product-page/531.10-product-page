import { Col, Container, Row, Breadcrumb, BreadcrumbItem} from "reactstrap";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import FilterTable from "../Components/filterComponent/filter";
import { useSelector } from "react-redux";
import { Grid, Pagination } from "@mui/material";
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    return data;
}
const MoreProduct = () =>{
    const navigate = useNavigate();
    const [currentPage, getCurrentPage] = useState(1);

    const {condition, status} = useSelector((reduxData) =>
        reduxData.productReducer
    )
    
    const [Product, showProduct] = useState([]);
    const [length, getLength] = useState(0);
   
    
    //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    useEffect(()=>{
        navigate("/Categories/More");
        if(status === true){
            fetchApi("http://localhost:8000/api/products/filter?"+"description="+`${condition.description}`+"&"+"type="+`${condition.type}`+"&"+"minPrice="+`${condition.minPrice}`+"&"+"maxPrice="+`${condition.maxPrice}`)
                .then((data) =>{
                    showProduct(data.data);
                    getLength(data.data.length);
                })
                .catch((error)=>{
                    console.log(error.message)
                })
            };
        if(status=== false){
            fetchApi("http://localhost:8000/api/skip-limit-products?skipNumber="+((currentPage-1) * limit)+"&limitNumber=" + limit)
                .then((data) =>{
                    showProduct(data.data);
                })
                .catch((error)=>{
                    console.log(error.message)
                })
                //lấy length của data để phân trang
            fetchApi("http://localhost:8000/api/skip-limit-products?")
                .then((data) =>{
                    getLength(data.data.length);
                })
                .catch((error)=>{
                    console.log(error.message)
                })
        }
    },[currentPage, status])

    const limit = 6;
    const totalProduct = length;
    const noPage = Math.ceil(totalProduct / limit);
    const onChangePagination = (event, value) => {
        getCurrentPage(value);
    }
   
    return(
        <Container>
            <Breadcrumb>
                <BreadcrumbItem > 
                <a href="/" style={{textDecoration:"none"}}>
                    Home
                </a>
                </BreadcrumbItem>
                <BreadcrumbItem > 
                <a href="/Categories" style={{textDecoration:"none"}}>
                    Categories
                </a>
                </BreadcrumbItem>
                <BreadcrumbItem active> 
                <a href="/Categories/More">
                    Products
                </a>
                </BreadcrumbItem>
            </Breadcrumb>
            
            <Col>
                <Col style={{marginTop:"50px", marginBottom:"10px"}}className="d-flex align-items-center justify-content-center">
                    <Row >
                        <Col style={{marginRight:"20px"}}>
                            <h4>Products</h4>
                            
                        </Col>
                        <Col>
                            <hr style={{width:"50px",border:"1px solid orange", marginLeft:"-70%"}}/>
                        </Col>
                    </Row>
                </Col>
            </Col>  
                <Row xs="12">
                    <Col xs="3" style={{marginTop:"6%"}}>
                        <FilterTable/>
                    </Col>
                    <Col xs="9" className="d-flex flex-wrap">
                        {Product? 
                            Product.map((value,index) => {
                                return (
                                    
                                        <Col sm={4} key={index}>
                                            <div className="text-center img-product">
                                                <img src={value.ImageUrl} style={{width:"300px", height:"300px"}} alt="img"/>
                                                <div style={{marginTop:"15px"}}>
                                                    <h6 style={{margin:"0px"}}>{value.Name}</h6>
                                                    <p style={{margin:"0px"}}>Type: <b>{value.Type}</b></p>
                                                    <p style={{marginTop:"-5px"}}><b>${value.PromotionPrice}</b></p>
                                                </div>
                                            </div>
                                        </Col>
                                    )
                                }) 
                            : null}
                    </Col>
                </Row>
                <Grid item md={12} sm={12} lg={12} xs={12} mt={5} mb={5}
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center">
                    <Pagination count={noPage} defaultPage={currentPage} onChange={onChangePagination}/>
                </Grid>
        </Container>
    )
}
export default MoreProduct;