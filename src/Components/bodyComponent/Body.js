import AboutUs from "./bodyContent/aboutUs";
import SliderCaroul from "./bodyContent/Casouline";
import Categories from "./bodyContent/Categories";
import LastestProduct from "./bodyContent/LastestProduct";

const BodyComponent = () => {
    return(
        <div>
            <SliderCaroul/>
            <LastestProduct/>
            <Categories/>
            <AboutUs/>
        </div>
        )
}
export default BodyComponent;