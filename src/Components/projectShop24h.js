import BodyComponent from "./bodyComponent/Body";
import Footer from "./footerComponent/Footer";
import Header from "./headerComponent/Header";

const Shop24h = () => {
    return(
        <>
        <Header/>
        <BodyComponent/>
        <Footer/>
        </>
    )
}
export default Shop24h;