import { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Input, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';

import auth from '../../firebase';
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from 'firebase/auth';

const provider = new GoogleAuthProvider();
function LoginModal(args) {
  const [open, setOpen] = useState(false);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggleAvatar = () => setDropdownOpen(!dropdownOpen);
  const toggle = () => setOpen(!open);
    
    //login gg
    const [user, setUser] = useState(null);
    const onLogInButton = () =>{
        signInWithPopup(auth, provider)
        .then((result) =>{
            setUser(result.user)
        })
        .catch((error)=>{
            console.error(error)
        });
        setOpen(!open)
    }
    //log out 
    const logoutGoogle = () => {
        signOut(auth)
          .then(() => {
            setUser(null);
          })
          .catch((error) => {
            console.error(error);
          })
      }
    //giữ lại giá trị user khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
          setUser(result);
        })
      })
  return (
    <>  
        {user? 
        <Row>
            <Col sm={3} className="d-flex align-items-center">
                <Dropdown nav isOpen={dropdownOpen} toggle={toggleAvatar} style={{listStyleType:"none"}}>
                    <DropdownToggle nav >
                        <img src={user.photoURL} referrerpolicy="no-referrer" width={30} style={{borderRadius: "50%"}}/>
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem>
                            <Button style={{backgroundColor:"#d1c286"}} onClick={logoutGoogle}>Log out</Button>
                        </DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </Col>
        </Row> 
            : 
        <div>
            <i className="fa-solid fa-user" onClick={toggle}></i>
            <Modal isOpen={open} toggle={toggle} {...args}>
                <ModalHeader toggle={toggle} style={{color:"#d1c286"}}>Sign In</ModalHeader>
                <ModalBody >
                    <Row >
                        <Col className='d-flex justify-content-center' sm={12}>
                            <Button color='danger' style={{ width:"80%",borderRadius:"50px"}}
                                onClick={onLogInButton}>
                            <i className="fa-brands fa-google"/> Sign In with <b>Google</b> 
                            </Button>
                        </Col>
                        <Col className='d-flex justify-content-center' sm={12}>
                        <div><hr style={{width:"150px",border:"1px solid black"}}/></div>
                        </Col>
                        <Col sm={12} className='d-flex flex-column align-items-center'>
                            <FormGroup style={{width:"80%"}} sm={12}>
                                <Input
                                name="username"
                                placeholder="Username"
                                style={{borderRadius:"50px"}}
                                />
                            </FormGroup>
                            <FormGroup style={{width:"80%"}}>
                                <Input
                                name="password"
                                placeholder="Password"
                                type="password"
                                style={{borderRadius:"50px"}}
                                />
                            </FormGroup>
                            <Button color='success' style={{ width:"80%",borderRadius:"50px"}}>
                                Sign In  
                            </Button>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Col className='text-center'>
                        <p> Don't have an account?  <p  style={{color:"green"}}>Sign up here</p></p>
                    </Col>
                </ModalFooter>
        </Modal>
      </div>
    }
    </>  
    
  );
}

export default LoginModal;